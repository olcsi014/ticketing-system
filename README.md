# Ticketing System

!! _A kód maga a main brachre került feltöltésre_ !!

**--Installációs guide--**

szükséges eszközök: npm, node

**pull** request után commandline-ba a '_ticket-server_' és a '_ticket-react_' mappákban lefuttatni az '`npm init`' parancsot ami a szükséges csomagfájlokat letölti az adott gépre

ezután a '_ticket-server_' mappában az '`npm run start:dev`' utasítással müködésbe lép és a 8000-res porton figyel
a 'ticket-react' mappában az '`npm start`' utasítással elindul a kliens alkalmazás a 3000-res porton

**-- Szerver --**

A szerver a megbeszéltek alapján nestjs felhasználásával készült. A feladatok elvégzéséhez készült API, ami különböző útvonalakon keresztül szolgáltatja az erőforrásokat

**Útvonalak:**

Felhasználók:
- GET /users - Felhasználók listázása
- GET /users/:id - Felhasználó lekérdezése id alapján
- POST /users - Felhasználó létrehozása
- POST /users/login - Felhasználó bejelentkeztetése
- PATCH /users/:id - Felhasználó módosítása id alapján
- DELETE /users/:id - Törlés id alapján


Hibajegyek:
- GET /tickets - Hibajegyek listázása
- GET /tickets/:id - Hibajegy lekérdezése id alapján
- POST /tickets - Hibajegy létrehozása
- PATCH /tickets/:id - Hibajegy módosítása id alapján
- DELETE /tickets/:id - Törlés id alapján


Jogosultságok:
- GET /roles - Jogosultságok lekérdezése
- POST /roles - Felhasználó jogosultságának változtatása



** --Kliens-- **

A Kliens ahhoz hogy a felhasználó elkezdhessen dolgozni egy alap authentikációt kér, ami után hozzáférhet a jogosultságának megfelelően a hibajegyekhez és a felhasználókhoz.

Funkcionalitásában még rendkívül kezdetleges

